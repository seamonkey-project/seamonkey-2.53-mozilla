# -*- Mode: python; indent-tabs-mode: nil; tab-width: 40 -*-
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

include('../js-config.mozbuild')
include('../js-cxxflags.mozbuild')

FINAL_LIBRARY = "js"

# Includes should be relative to parent path
LOCAL_INCLUDES += ["!..", ".."]

CXXFLAGS += ["-Wno-error=type-limits"]

SOURCES += [
    'imported/regexp-ast.cc',
    'imported/regexp-bytecode-generator.cc',
    'imported/regexp-bytecode-peephole.cc',
    'imported/regexp-bytecodes.cc',
    'imported/regexp-compiler-tonode.cc',
    'imported/regexp-compiler.cc',
    'imported/regexp-dotprinter.cc',
    'imported/regexp-interpreter.cc',
    'imported/regexp-macro-assembler-tracer.cc',
    'imported/regexp-macro-assembler.cc',
    'imported/regexp-parser.cc',
    'imported/regexp-stack.cc',
    'RegExpAPI.cpp',
    'RegExpNativeMacroAssembler.cpp',
    'RegExpShim.cpp',
    'util/UnicodeShim.cpp'
]

if CONFIG['JS_HAS_INTL_API']:
    CXXFLAGS += ['-DV8_INTL_SUPPORT']
    SOURCES += [
        'imported/property-sequences.cc',
        'imported/special-case.cc'
    ]

if CONFIG['_MSC_VER']:
    # This is intended as a temporary workaround to unblock compilation
    # on VS2015 in warnings as errors mode.
    CXXFLAGS += ['-wd4275']
