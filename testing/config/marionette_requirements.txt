-r mozbase_requirements.txt

../tools/wptserve
../marionette/client
../marionette/harness

# Allows to use the Puppeteer page object model for Firefox
../marionette/puppeteer/firefox/
