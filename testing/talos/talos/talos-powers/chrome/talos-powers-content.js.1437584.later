--- talos-powers-content.js
+++ talos-powers-content.js
@@ -1,42 +1,36 @@
 /* This Source Code Form is subject to the terms of the Mozilla Public
  * License, v. 2.0. If a copy of the MPL was not distributed with this
  * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
 
 // This file is loaded as a framescript
+/* global docShell */
+// eslint-env mozilla/frame-script
 
-/* globals docShell */
+ChromeUtils.import("resource://gre/modules/Services.jsm");
 
 function canQuitApplication() {
-  var os = Cc["@mozilla.org/observer-service;1"]
-    .getService(Ci.nsIObserverService);
-  if (!os) {
-    return true;
-  }
-
   try {
     var cancelQuit = Cc["@mozilla.org/supports-PRBool;1"]
       .createInstance(Ci.nsISupportsPRBool);
-    os.notifyObservers(cancelQuit, "quit-application-requested");
+    Services.obs.notifyObservers(cancelQuit, "quit-application-requested");
 
     // Something aborted the quit process.
     if (cancelQuit.data) {
       return false;
     }
   } catch (ex) {
   }
-  os.notifyObservers(null, "quit-application-granted");
+  Services.obs.notifyObservers(null, "quit-application-granted");
   return true;
 }
 
 function goQuitApplication(waitForSafeBrowsing) {
-  var xulRuntime = Cc["@mozilla.org/xre/app-info;1"]
-                 .getService(Ci.nsIXULRuntime);
-  if (xulRuntime.processType == xulRuntime.PROCESS_TYPE_CONTENT) {
+  if (Services.appinfo.processType == Services.appinfo.PROCESS_TYPE_CONTENT) {
     // If we're running in a remote browser, emit an event for a
     // frame script to pick up to quit the whole browser.
     var event = new content.CustomEvent("TalosQuitApplication", {bubbles: true, detail: {waitForSafeBrowsing}});
     content.document.dispatchEvent(event);
     return false;
   }
 
   if (waitForSafeBrowsing) {
@@ -56,22 +50,19 @@ function goQuitApplication(waitForSafeBr
     return false;
   }
 
   const kAppStartup = "@mozilla.org/toolkit/app-startup;1";
   const kAppShell   = "@mozilla.org/appshell/appShellService;1";
   var appService;
 
   if (kAppStartup in Cc) {
-    appService = Cc[kAppStartup].
-      getService(Ci.nsIAppStartup);
-
+    appService = Services.startup;
   } else if (kAppShell in Cc) {
-    appService = Cc[kAppShell].
-      getService(Ci.nsIAppShellService);
+    appService = Services.appShell;
   } else {
     throw "goQuitApplication: no AppStartup/appShell";
   }
 
   var windowManager = Components.
     classes["@mozilla.org/appshell/window-mediator;1"].getService();
 
   var windowManagerInterface = windowManager.
