;; This Source Code Form is subject to the terms of the Mozilla Public
;; License, v. 2.0. If a copy of the MPL was not distributed with this
;; file, You can obtain one at http://mozilla.org/MPL/2.0/.

    %define DATA_SYMBOL ICU_DATA_SYMBOL

    global DATA_SYMBOL

SECTION .rodata align=16
DATA_SYMBOL:
        incbin ICU_DATA_FILE
