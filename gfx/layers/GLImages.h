/* -*- Mode: C++; tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/* vim: set ts=8 sts=2 et sw=2 tw=80: */
/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef GFX_GLIMAGES_H
#define GFX_GLIMAGES_H

#include "AndroidSurfaceTexture.h"
#include "GLContextTypes.h"
#include "GLTypes.h"
#include "ImageContainer.h"             // for Image
#include "ImageTypes.h"                 // for ImageFormat::SHARED_GLTEXTURE
#include "nsCOMPtr.h"                   // for already_AddRefed
#include "mozilla/gfx/Point.h"          // for IntSize

namespace mozilla {
namespace layers {

class GLImage : public Image
{
public:
  explicit GLImage(ImageFormat aFormat) : Image(nullptr, aFormat){}

  already_AddRefed<gfx::SourceSurface> GetAsSourceSurface() override;

  GLImage* AsGLImage() override { return this; }
};

} // namespace layers
} // namespace mozilla

#endif // GFX_GLIMAGES_H
