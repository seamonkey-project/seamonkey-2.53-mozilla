/* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef TypeInState_h
#define TypeInState_h

#include "mozilla/UniquePtr.h"
#include "nsCOMPtr.h"
#include "nsCycleCollectionParticipant.h"
#include "nsISupportsImpl.h"
#include "nsString.h"
#include "nsTArray.h"
#include "nscore.h"

// Workaround for windows headers
#ifdef SetProp
#undef SetProp
#endif

class nsIAtom;
class nsIDOMNode;

namespace mozilla {

class HTMLEditRules;
namespace dom {
class Selection;
} // namespace dom

struct PropItem
{
  nsIAtom* tag;
  nsIAtom* attr;
  nsString value;

  PropItem();
  PropItem(nsIAtom* aTag, nsIAtom* aAttr, const nsAString& aValue);
  ~PropItem();
};

class TypeInState final
{
public:
  NS_INLINE_DECL_CYCLE_COLLECTING_NATIVE_REFCOUNTING(TypeInState)
  NS_DECL_CYCLE_COLLECTION_NATIVE_CLASS(TypeInState)

  TypeInState();
  void Reset();

  nsresult UpdateSelState(dom::Selection* aSelection);

  void OnSelectionChange(dom::Selection& aSelection);

  void SetProp(nsIAtom* aProp, nsIAtom* aAttr, const nsAString& aValue);

  void ClearAllProps();
  void ClearProp(nsIAtom* aProp, nsIAtom* aAttr);

  /**
   * TakeClearProperty() hands back next property item on the clear list.
   * Caller assumes ownership of PropItem and must delete it.
   */
  UniquePtr<PropItem> TakeClearProperty();

  /**
   * TakeSetProperty() hands back next property item on the set list.
   * Caller assumes ownership of PropItem and must delete it.
   */
  UniquePtr<PropItem> TakeSetProperty();

  /**
   * TakeRelativeFontSize() hands back relative font value, which is then
   * cleared out.
   */
  int32_t TakeRelativeFontSize();

  void GetTypingState(bool& isSet, bool& theSetting, nsIAtom* aProp,
                      nsIAtom* aAttr = nullptr, nsString* outValue = nullptr);

  static bool FindPropInList(nsIAtom* aProp, nsIAtom* aAttr,
                             nsAString* outValue, nsTArray<PropItem*>& aList,
                             int32_t& outIndex);

protected:
  virtual ~TypeInState();

  void RemovePropFromSetList(nsIAtom* aProp, nsIAtom* aAttr);
  void RemovePropFromClearedList(nsIAtom* aProp, nsIAtom* aAttr);
  bool IsPropSet(nsIAtom* aProp, nsIAtom* aAttr, nsAString* outValue);
  bool IsPropSet(nsIAtom* aProp, nsIAtom* aAttr, nsAString* outValue,
                 int32_t& outIndex);
  bool IsPropCleared(nsIAtom* aProp, nsIAtom* aAttr);
  bool IsPropCleared(nsIAtom* aProp, nsIAtom* aAttr, int32_t& outIndex);

  nsTArray<PropItem*> mSetArray;
  nsTArray<PropItem*> mClearedArray;
  int32_t mRelativeFontSize;
  nsCOMPtr<nsIDOMNode> mLastSelectionContainer;
  int32_t mLastSelectionOffset;

  friend class HTMLEditRules;
};

} // namespace mozilla

#endif  // #ifndef TypeInState_h

