"use strict";

/* globals browser */

ChromeUtils.defineModuleGetter(this, "AddonManager",
                               "resource://gre/modules/AddonManager.jsm");

add_task(async function setup() {
  await ExtensionTestUtils.startAddonManager();
});

