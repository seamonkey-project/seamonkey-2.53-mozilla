   * - pylint
     -
     - `bug 1623024 <https://bugzilla.mozilla.org/show_bug.cgi?id=1623024>`_
     - :ref:`pylint`
     - https://www.pylint.org/
   * - Python 2/3 compatibility check