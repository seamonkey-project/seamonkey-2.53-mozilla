PRStatus
========

Type for status code returned by some functions.

.. _Syntax:

Syntax
------

.. code:: eval

   #include <prtypes.h>

   typedef enum { PR_FAILURE = -1, PR_SUCCESS = 0 } PRStatus;
