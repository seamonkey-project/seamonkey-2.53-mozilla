PRProcess
=========

Represents a process.

.. _Syntax:

Syntax
------

::

   #include <prproces.h>

   typedef struct PRProcess PRProcess;

.. _Description:

Description
-----------

A pointer to the opaque :ref:`PRProcess` structure identifies a process.
