PR_GetThreadPrivate
===================

Recovers the per-thread private data for the current thread.

.. _Syntax:

Syntax
------

.. code:: eval

   #include <prthread.h>

   void* PR_GetThreadPrivate(PRUintn index);

.. _Parameter:

Parameter
~~~~~~~~~

:ref:`PR_GetThreadPrivate` has the following parameters:

``index``
   The index into the per-thread private data table.

.. _Returns:

Returns
~~~~~~~

``NULL`` if the data has not been set.

.. _Description:

Description
-----------

:ref:`PR_GetThreadPrivate` may be called at any time during a thread's
execution. A thread can get access only to its own per-thread private
data. Do not delete the object that the private data refers to without
first clearing the thread's value.
