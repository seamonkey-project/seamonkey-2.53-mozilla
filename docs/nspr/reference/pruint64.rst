PRUint64
========

Guaranteed to be an unsigned 64-bit integer on all platforms.

.. _Syntax:

Syntax
------

.. code:: eval

   #include <prtypes.h>

   typedef definition PRUint64;

.. _Description:

Description
-----------

May be defined in several different ways, depending on the platform. For
syntax details for each platform, see
`prtypes.h <https://dxr.mozilla.org/mozilla-central/source/nsprpub/pr/include/prtypes.h>`__.
