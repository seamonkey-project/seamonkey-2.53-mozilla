PRPrimordialFn
==============

The type for the root function used by :ref:`PR_Initialize` is specified as
follows:

.. _Syntax:

Syntax
------

.. code:: eval

   typedef PRIntn (PR_CALLBACK *PRPrimordialFn)(PRIntn argc, char **argv);

.. _See_Also:

See Also
--------

 - :ref:`PR_Initialize`
