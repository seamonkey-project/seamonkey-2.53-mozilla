PRInt16
=======

Guaranteed to be a signed 16-bit integer on all platforms.

.. _Syntax:

Syntax
------

::

   #include <prtypes.h>

   typedefdefinition PRInt16;
