PRInt8
======

Guaranteed to be a signed 8-bit integer on all platforms.

.. _Syntax:

Syntax
------

.. code:: eval

   #include <prtypes.h>

   typedef definition PRInt8;
