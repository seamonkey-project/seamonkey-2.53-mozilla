PRJobFn
=======

.. _Syntax:

Syntax
------

.. code:: eval

   #include <prtpool.h>

   typedef void (PR_CALLBACK *PRJobFn)(void *arg);
