PRUint8
=======

Guaranteed to be an unsigned 8-bit integer on all platforms. There is no
type equivalent to a plain ``char``.

.. _Syntax:

Syntax
------

.. code:: eval

   #include <prtypes.h>

   typedefdefinition PRUint8;
