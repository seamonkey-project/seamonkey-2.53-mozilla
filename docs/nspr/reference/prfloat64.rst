
PRFloat64
=========

The NSPR floating-point type is always 64 bits.

.. _Syntax:

Syntax
------

.. code:: eval

   #include <prtypes.h>

   typedef double PRFloat64;
