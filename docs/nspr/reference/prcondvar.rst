PRCondVar
=========

Structure for a condition variable.

.. _Syntax:

Syntax
------

.. code:: eval

   #include <prcvar.h>

   typedef struct PRCondVar PRCondVar;

.. _Description:

Description
-----------

An NSPR condition variable is an opaque object identified by a pointer.
