PR_REMOVE_LINK
==============

Removes an element from a circular list.

.. _Syntax:

Syntax
------

.. code:: eval

   #include <prclist.h>

   PR_REMOVE_LINK (PRCList *elemp);

.. _Parameter:

Parameter
~~~~~~~~~

``elemp``
   A pointer to the element.

.. _Description:

Description
-----------

:ref:`PR_REMOVE_LINK` removes the specified element from its circular list.
