PRUint16
========

Guaranteed to be an unsigned 16-bit integer on all platforms.

.. _Syntax:

Syntax
------

.. code:: eval

   #include <prtypes.h>

   typedefdefinition PRUint16;
