PR_VERSION
==========

.. _Syntax:

Syntax
------

.. code:: eval

   #include <prinit.h>

   #define PR_VERSION "2.1 yyyymmdd"

.. _Description:

Description
-----------

The format of the version string is\ *MajorVersion.MinorVersion
BuildDate*.
