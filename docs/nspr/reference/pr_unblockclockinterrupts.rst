PR_UnblockClockInterrupts
=========================

Unblocks the timer signal used for preemptive scheduling.

.. _Syntax:

Syntax
------

.. code:: eval

   #include <prinit.h>

   void PR_UnblockClockInterrupts(void);
