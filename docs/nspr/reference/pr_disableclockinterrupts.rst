PR_DisableClockInterrupts
=========================

Disables timer signals used for preemptive scheduling.

.. _Syntax:

Syntax
------

.. code:: eval

   #include <prinit.h>

   void PR_DisableClockInterrupts(void);
