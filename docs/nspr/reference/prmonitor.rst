PRMonitor
=========

An opaque structure managed entirely by the client. Clients create them
when needed and must destroy them when no longer needed.

.. _Syntax:

Syntax
------

.. code:: eval

   #include <prmon.h>

   typedef struct PRMonitor PRMonitor;
