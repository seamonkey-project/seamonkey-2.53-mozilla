PRPackedBool
============

Packed Boolean value.

.. _Syntax:

Syntax
------

.. code:: eval

   #include <prtypes.h>

   typedef PRUint8 PRPackedBool;

.. _Description:

Description
-----------

Use :ref:`PRPackedBool` within structures where bit fields are not desirable but minimum and consistent overhead matters.
