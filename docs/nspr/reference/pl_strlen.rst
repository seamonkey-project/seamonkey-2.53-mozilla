PL_strlen
=========

Returns the length of a specified string (not including the trailing
``'\0'``)

.. _Syntax:

Syntax
~~~~~~

.. code:: eval

   PRUint32 PL_strlen(const char *str);

.. _Parameter:

Parameter
~~~~~~~~~

The function has these parameter:

``str``
   Size in bytes of item to be allocated.

.. _Returns:

Returns
~~~~~~~

If successful, the function returns length of the specified string.
