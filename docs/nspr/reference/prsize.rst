PRSize
======

A type for representing the size of an object (not the size of a
pointer). This is the same as the corresponding type in ``libc``.

.. _Syntax:

Syntax
------

.. code:: eval

   #include <prtypes.h>

   typedef size_t PRSize;
