PR_INIT_CLIST
=============

Initializes a circular list.

.. _Syntax:

Syntax
------

.. code:: eval

   #include <prclist.h>

   PR_INIT_CLIST (PRCList *listp);

.. _Parameter:

Parameter
~~~~~~~~~

``listp``
   A pointer to the anchor of the linked list.

.. _Description:

Description
-----------

Initializes the specified list to be an empty list.
