PRUptrdiff
==========

Unsigned pointer difference type.

.. _Syntax:

Syntax
------

.. code:: eval

   #include <prtypes.h>

   typedef unsigned long PRUptrdiff;
