PRPtrdiff
=========

Signed pointer difference type.

.. _Syntax:

Syntax
------

.. code:: eval

   #include <prtypes.h>

   typedef ptrdiff_t PRPtrdiff;
