PR_GetThreadPriority
====================

Returns the priority of a specified thread.

.. _Syntax:

Syntax
------

.. code:: eval

   #include <prthread.h>

   PRThreadPriority PR_GetThreadPriority(PRThread *thread);

.. _Parameter:

Parameter
~~~~~~~~~

:ref:`PR_GetThreadPriority` has the following parameter:

``thread``
   A valid identifier for the thread whose priority you want to know.
