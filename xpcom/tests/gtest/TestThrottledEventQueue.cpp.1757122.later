--- TestThrottledEventQueue.cpp
+++ TestThrottledEventQueue.cpp
@@ -25,17 +25,16 @@
 #include "prinrval.h"
 
 using mozilla::CondVar;
 using mozilla::MakeRefPtr;
 using mozilla::Mutex;
 using mozilla::MutexAutoLock;
 using mozilla::ThrottledEventQueue;
 using std::function;
-using std::move;
 using std::string;
 
 namespace TestThrottledEventQueue {
 
 // A simple queue of runnables, to serve as the base target of
 // ThrottledEventQueues in tests.
 //
 // This is much simpler than mozilla::TaskQueue, and so better for unit tests.
@@ -44,17 +43,17 @@ namespace TestThrottledEventQueue {
 struct RunnableQueue : nsISerialEventTarget {
   std::queue<nsCOMPtr<nsIRunnable>> runnables;
 
   bool IsEmpty() { return runnables.empty(); }
   size_t Length() { return runnables.size(); }
 
   [[nodiscard]] nsresult Run() {
     while (!runnables.empty()) {
-      auto runnable = move(runnables.front());
+      auto runnable = std::move(runnables.front());
       runnables.pop();
       nsresult rv = runnable->Run();
       if (NS_FAILED(rv)) return rv;
     }
 
     return NS_OK;
   }
 
@@ -92,18 +91,18 @@ struct RunnableQueue : nsISerialEventTar
 
  private:
   virtual ~RunnableQueue() = default;
 };
 
 NS_IMPL_ISUPPORTS(RunnableQueue, nsIEventTarget, nsISerialEventTarget)
 
 static void Enqueue(nsIEventTarget* target, function<void()>&& aCallable) {
-  nsresult rv =
-      target->Dispatch(NS_NewRunnableFunction("TEQ GTest", move(aCallable)));
+  nsresult rv = target->Dispatch(
+      NS_NewRunnableFunction("TEQ GTest", std::move(aCallable)));
   MOZ_ALWAYS_TRUE(NS_SUCCEEDED(rv));
 }
 
 }  // namespace TestThrottledEventQueue
 
 using namespace TestThrottledEventQueue;
 
 TEST(ThrottledEventQueue, RunnableQueue)
