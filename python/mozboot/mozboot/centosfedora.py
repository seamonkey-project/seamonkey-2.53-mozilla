# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this file,
# You can obtain one at http://mozilla.org/MPL/2.0/.

from __future__ import absolute_import, print_function, unicode_literals

from mozboot.base import BaseBootstrapper
from mozboot.linux_common import LinuxBootstrapper


class CentOSFedoraBootstrapper(
        LinuxBootstrapper,
        BaseBootstrapper):
    def __init__(self, distro, version, dist_id, **kwargs):
        BaseBootstrapper.__init__(self, **kwargs)

        self.distro = distro
        self.version = int(version.split('.')[0])
        self.dist_id = dist_id

        self.group_packages = []

        self.packages = [
            'nodejs',
            'npm',
            'which',
        ]

        self.browser_group_packages = [
            'GNOME Software Development',
        ]

        self.browser_packages = [
            'alsa-lib-devel',
            'dbus-glib-devel',
            'glibc-static',
            'libstdc++-static',
            'libXt-devel',
            'nasm',
            'pulseaudio-libs-devel',
            'wireless-tools-devel',
        ]

        self.mobile_android_packages = [
            'java-1.8.0-openjdk-devel',
            # For downloading the Android SDK and NDK.
            'wget',
        ]

        if self.distro in ('centos'):
            self.group_packages += [
                'Development Tools',
            ]

            self.packages += [
                'gcc-c++',
                'curl-devel',
            ]

            self.browser_packages += [
                'gtk3-devel',
            ]

            if self.version == 6:
                self.group_packages += [
                    'Development Libraries',
                    'GNOME Software Development',
                ]

            elif self.version == 7:

                self.browser_group_packages = [
                    'Development Tools',
                ]

        elif self.distro == 'fedora':
            self.group_packages += [
                'C Development Tools and Libraries',
            ]

            self.packages += [
                'redhat-rpm-config',
            ]

            self.browser_packages += [
                'gcc-c++',
                'python-dbus',
            ]

            self.mobile_android_packages += [
                'ncurses-compat-libs',
            ]

    def install_system_packages(self):
        self.dnf_groupinstall(*self.group_packages)
        self.dnf_install(*self.packages)

    def install_browser_packages(self):
        self.ensure_browser_packages()

    def install_browser_artifact_mode_packages(self):
        self.ensure_browser_packages(artifact_mode=True)

    def install_mobile_android_packages(self):
        self.ensure_mobile_android_packages(artifact_mode=False)

    def install_mobile_android_artifact_mode_packages(self):
        self.ensure_mobile_android_packages(artifact_mode=True)

    def ensure_browser_packages(self, artifact_mode=False):
        # TODO: Figure out what not to install for artifact mode
        self.dnf_groupinstall(*self.browser_group_packages)
        self.dnf_install(*self.browser_packages)

    def ensure_mobile_android_packages(self, artifact_mode=False):
        # Install Android specific packages.
        self.dnf_install(*self.mobile_android_packages)

        self.ensure_java()
        from mozboot import android
        android.ensure_android('linux', artifact_mode=artifact_mode,
                               no_interactive=self.no_interactive)

    def generate_mobile_android_mozconfig(self, artifact_mode=False):
        from mozboot import android
        return android.generate_mozconfig('linux', artifact_mode=artifact_mode)

    def generate_mobile_android_artifact_mode_mozconfig(self):
        return self.generate_mobile_android_mozconfig(artifact_mode=True)

    def upgrade_mercurial(self, current):
        self.dnf_update('mercurial')
