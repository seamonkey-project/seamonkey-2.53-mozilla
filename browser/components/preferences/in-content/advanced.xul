# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

<!-- Advanced panel -->

<script type="application/javascript"
        src="chrome://browser/content/preferences/in-content/advanced.js"/>

<preferences id="advancedPreferences" hidden="true" data-category="paneAdvanced">
  <preference id="browser.preferences.advanced.selectedTabIndex"
              name="browser.preferences.advanced.selectedTabIndex"
              type="int"/>

  <!-- General tab -->
  <preference id="accessibility.browsewithcaret"
              name="accessibility.browsewithcaret"
              type="bool"/>
  <preference id="accessibility.typeaheadfind"
              name="accessibility.typeaheadfind"
              type="bool"/>
  <preference id="accessibility.blockautorefresh"
              name="accessibility.blockautorefresh"
              type="bool"/>
#ifdef XP_WIN
  <preference id="ui.osk.enabled"
              name="ui.osk.enabled"
              type="bool"/>
#endif

  <preference id="general.autoScroll"
              name="general.autoScroll"
              type="bool"/>
  <preference id="general.smoothScroll"
              name="general.smoothScroll"
              type="bool"/>
  <preference id="layout.spellcheckDefault"
              name="layout.spellcheckDefault"
              type="int"/>

  <!-- Data Choices tab -->
#ifdef MOZ_CRASHREPORTER
  <preference id="browser.crashReports.unsubmittedCheck.autoSubmit2"
              name="browser.crashReports.unsubmittedCheck.autoSubmit2"
              type="bool"/>
#endif

 <!-- Update tab -->
#ifdef MOZ_UPDATER
  <preference id="app.update.enabled"
              name="app.update.enabled"
              type="bool"/>
  <preference id="app.update.auto"
              name="app.update.auto"
              type="bool"/>

  <preference id="app.update.disable_button.showUpdateHistory"
              name="app.update.disable_button.showUpdateHistory"
              type="bool"/>

#ifdef MOZ_MAINTENANCE_SERVICE
  <preference id="app.update.service.enabled"
              name="app.update.service.enabled"
              type="bool"/>
#endif
#endif

  <preference id="browser.search.update"
              name="browser.search.update"
              type="bool"/>

  <!-- Certificates tab -->
  <preference id="security.default_personal_cert"
              name="security.default_personal_cert"
              type="string"/>

  <preference id="security.disable_button.openCertManager"
              name="security.disable_button.openCertManager"
              type="bool"/>

  <preference id="security.disable_button.openDeviceManager"
              name="security.disable_button.openDeviceManager"
              type="bool"/>

  <preference id="security.OCSP.enabled"
              name="security.OCSP.enabled"
              type="int"/>
</preferences>

#ifdef HAVE_SHELL_SERVICE
  <stringbundle id="bundleShell" src="chrome://browser/locale/shellservice.properties"/>
  <stringbundle id="bundleBrand" src="chrome://branding/locale/brand.properties"/>
#endif
  <stringbundle id="bundlePreferences" src="chrome://browser/locale/preferences-old/preferences.properties"/>

<hbox id="header-advanced"
      class="header"
      hidden="true"
      data-category="paneAdvanced">
  <label class="header-name" flex="1">&paneAdvanced.title;</label>
  <html:a class="help-button" target="_blank" aria-label="&helpButton.label;"></html:a>
</hbox>

<tabbox id="advancedPrefs"
        handleCtrlTab="false"
        handleCtrlPageUpDown="false"
        flex="1"
        data-category="paneAdvanced"
        hidden="true">

  <tabs id="tabsElement">
    <tab id="generalTab" label="&generalTab.label;"/>
#ifdef MOZ_DATA_REPORTING
    <tab id="dataChoicesTab" label="&dataChoicesTab.label;"/>
#endif
    <tab id="networkTab" label="&networkTab.label;"/>
    <tab id="updateTab" label="&updateTab.label;"/>
    <tab id="encryptionTab" label="&certificateTab.label;"/>
  </tabs>

  <tabpanels flex="1">

    <!-- General -->
    <tabpanel id="generalPanel" orient="vertical">
      <!-- Accessibility -->
      <groupbox id="accessibilityGroup" align="start">
        <caption><label>&accessibility.label;</label></caption>

#ifdef XP_WIN
        <checkbox id="useOnScreenKeyboard"
                  hidden="true"
                  label="&useOnScreenKeyboard.label;"
                  accesskey="&useOnScreenKeyboard.accesskey;"
                  preference="ui.osk.enabled"/>
#endif
        <checkbox id="useCursorNavigation"
                  label="&useCursorNavigation.label;"
                  accesskey="&useCursorNavigation.accesskey;"
                  preference="accessibility.browsewithcaret"/>
        <checkbox id="searchStartTyping"
                  label="&searchOnStartTyping.label;"
                  accesskey="&searchOnStartTyping.accesskey;"
                  preference="accessibility.typeaheadfind"/>
        <checkbox id="blockAutoRefresh"
                  label="&blockAutoReload.label;"
                  accesskey="&blockAutoReload.accesskey;"
                  preference="accessibility.blockautorefresh"/>
      </groupbox>
      <!-- Browsing -->
      <groupbox id="browsingGroup" align="start">
        <caption><label>&browsing.label;</label></caption>

        <checkbox id="useAutoScroll"
                  label="&useAutoScroll.label;"
                  accesskey="&useAutoScroll.accesskey;"
                  preference="general.autoScroll"/>
        <checkbox id="useSmoothScrolling"
                  label="&useSmoothScrolling.label;"
                  accesskey="&useSmoothScrolling.accesskey;"
                  preference="general.smoothScroll"/>
        <checkbox id="checkSpelling"
                  label="&checkUserSpelling.label;"
                  accesskey="&checkUserSpelling.accesskey;"
                  onsyncfrompreference="return gAdvancedPane.readCheckSpelling();"
                  onsynctopreference="return gAdvancedPane.writeCheckSpelling();"
                  preference="layout.spellcheckDefault"/>
      </groupbox>
    </tabpanel>
#ifdef MOZ_DATA_REPORTING
    <!-- Data Choices -->
    <tabpanel id="dataChoicesPanel" orient="vertical">
#ifdef MOZ_CRASHREPORTER
      <groupbox>
        <caption>
          <checkbox id="automaticallySubmitCrashesBox"
                    preference="browser.crashReports.unsubmittedCheck.autoSubmit2"
                    label="&alwaysSubmitCrashReports.label;"
                    accesskey="&alwaysSubmitCrashReports.accesskey;"/>
        </caption>
        <hbox class="indent" flex="1">
          <label flex="1">&crashReporterDesc2.label;</label>
          <label id="crashReporterLearnMore" flex="1"
                 class="learnMore text-link">&crashReporterLearnMore.label;</label>
        </hbox>
      </groupbox>
#endif
    </tabpanel>
#endif

    <!-- Network -->
    <tabpanel id="networkPanel" orient="vertical">

      <!-- Connection -->
      <groupbox id="connectionGroup">
        <caption><label>&connection.label;</label></caption>

        <hbox align="center">
          <description flex="1" control="connectionSettings">&connectionDesc.label;</description>
          <button id="connectionSettings" icon="network" label="&connectionSettings.label;"
                  accesskey="&connectionSettings.accesskey;"/>
        </hbox>
      </groupbox>

      <!-- Site Data -->
      <groupbox id="siteDataGroup">
        <caption><label>&siteData.label;</label></caption>

        <hbox align="baseline">
          <label id="totalSiteDataSize"></label>
          <label id="siteDataLearnMoreLink" class="learnMore text-link" value="&siteDataLearnMoreLink.label;"></label>
          <spacer flex="1" />
          <button id="clearSiteDataButton" icon="clear"
                  label="&clearSiteData.label;" accesskey="&clearSiteData.accesskey;"/>
        </hbox>
        <vbox align="end">
          <button id="siteDataSettings"
                  label="&siteDataSettings.label;"
                  accesskey="&siteDataSettings.accesskey;"/>
        </vbox>
      </groupbox>
    </tabpanel>

    <!-- Update -->
    <tabpanel id="updatePanel" orient="vertical">
#ifdef MOZ_UPDATER
      <groupbox id="updateApp" align="start">
        <caption><label>&updateApplication.label;</label></caption>
        <radiogroup id="updateRadioGroup" align="start">
          <radio id="autoDesktop"
                 value="auto"
                 label="&updateAuto1.label;"
                 accesskey="&updateAuto1.accesskey;"/>
          <radio value="checkOnly"
                label="&updateCheckChoose.label;"
                accesskey="&updateCheckChoose.accesskey;"/>
          <radio value="manual"
                label="&updateManual.label;"
                accesskey="&updateManual.accesskey;"/>
        </radiogroup>
        <separator class="thin"/>
        <hbox>
          <button id="showUpdateHistory"
                  label="&updateHistory.label;"
                  accesskey="&updateHistory.accesskey;"
                  preference="app.update.disable_button.showUpdateHistory"/>
        </hbox>

#ifdef MOZ_MAINTENANCE_SERVICE
        <checkbox id="useService"
                  label="&useService.label;"
                  accesskey="&useService.accesskey;"
                  preference="app.update.service.enabled"/>
#endif
      </groupbox>
#endif
      <groupbox id="updateOthers" align="start">
        <caption><label>&autoUpdateOthers.label;</label></caption>
        <checkbox id="enableSearchUpdate"
                  label="&enableSearchUpdate.label;"
                  accesskey="&enableSearchUpdate.accesskey;"
                  preference="browser.search.update"/>
      </groupbox>
    </tabpanel>

    <!-- Certificates -->
    <tabpanel id="encryptionPanel" orient="vertical">
      <groupbox id="certSelection" align="start">
        <caption><label>&certPersonal.label;</label></caption>
        <description id="CertSelectionDesc" control="certSelection">&certPersonal.description;</description>

        <!--
          The values on these radio buttons may look like l12y issues, but
          they're not - this preference uses *those strings* as its values.
          I KID YOU NOT.
        -->
        <radiogroup id="certSelection"
                    preftype="string"
                    preference="security.default_personal_cert"
                    aria-labelledby="CertSelectionDesc">
          <radio label="&selectCerts.auto;"
                 accesskey="&selectCerts.auto.accesskey;"
                 value="Select Automatically"/>
          <radio label="&selectCerts.ask;"
                 accesskey="&selectCerts.ask.accesskey;"
                 value="Ask Every Time"/>
        </radiogroup>
      </groupbox>
      <separator/>
      <checkbox id="enableOCSP"
                label="&enableOCSP.label;"
                accesskey="&enableOCSP.accesskey;"
                onsyncfrompreference="return gAdvancedPane.readEnableOCSP();"
                onsynctopreference="return gAdvancedPane.writeEnableOCSP();"
                preference="security.OCSP.enabled"/>
      <separator/>
      <hbox>
        <button id="viewCertificatesButton"
                flex="1"
                label="&viewCerts.label;"
                accesskey="&viewCerts.accesskey;"
                preference="security.disable_button.openCertManager"/>
        <button id="viewSecurityDevicesButton"
                flex="1"
                label="&viewSecurityDevices.label;"
                accesskey="&viewSecurityDevices.accesskey;"
                preference="security.disable_button.openDeviceManager"/>
        <hbox flex="10"/>
      </hbox>
    </tabpanel>
  </tabpanels>
</tabbox>
